import pandas as pd
from sklearn.ensemble import RandomForestClassifier
from sklearn.metrics import accuracy_score
import numpy as np
from numpy import ravel
from keras.layers import Input
from keras.models import Model
from keras.models import Sequential
from keras.layers import Dense, Activation
from keras import regularizers,optimizers
from sklearn import svm
import time
import theano
import theano.tensor as T
import lasagne

import os
import random
#from sknn import mlp
#from matplotlib import pyplot
from sklearn.model_selection import cross_val_score

def normalize_train(input_arr):
    eps = 0.00001
    mean = np.mean(input_arr,axis=0)
    adjusted = input_arr - mean
    std = np.std(adjusted,axis=0)
    std = std+(np.abs(std) < eps)
    normed = adjusted/std
    return normed,mean,std

def normalize_test(input_arr,means,stds):
    adjusted = input_arr - means
    normed = adjusted/stds
    return normed

def build_mlp(input_var=None,input_shape=(),hidden_dim=(),regression = False):
    input_new_shape = (None,input_shape[1])
    l_in = lasagne.layers.InputLayer(shape=input_new_shape,
                                     input_var=input_var)
    # l_in_drop = lasagne.layers.DropoutLayer(l_in, p=0.2)
    # l_hid1 = lasagne.layers.DenseLayer(
    #     l_in_drop, num_units=hidden_dim,
    #     nonlinearity=lasagne.nonlinearities.rectify,
    #     W=lasagne.init.GlorotUniform())
    # l_hid1_drop = lasagne.layers.DropoutLayer(l_hid1, p=0.5)
    #
    # l_hid2 = lasagne.layers.DenseLayer(
    #     l_hid1_drop, num_units=600,
    #     nonlinearity=lasagne.nonlinearities.rectify)
    #
    # l_hid2_drop = lasagne.layers.DropoutLayer(l_hid2, p=0.5)
    if not regression:
        l_out = lasagne.layers.DenseLayer(
            l_in, num_units=2,
            nonlinearity=lasagne.nonlinearities.softmax)
    else:
        l_out = lasagne.layers.DenseLayer(
            l_in, num_units=2,
            nonlinearity=lasagne.nonlinearities.linear)

    return l_out

def iterate_minibatches(inputs, targets, batchsize, shuffle=False):
    if batchsize < 0:
        yield inputs,targets
        return
    last_end = 0
    for end in range(batchsize,len(inputs),batchsize):
        yield inputs[last_end:end], targets[last_end:end]
        last_end = end

max_id =10000
def load_dataset(data_sets=[("Data/posdrugs/",1)],num_significant=200,shuffle = True):
    sample_files = []
    for i in range(len(data_sets)):
        sample_files +=  [ (data_sets[i][0]+path,data_sets[i][1])
                           for path in os.listdir(data_sets[i][0]) if path[0] not in ['.','_']]
    if shuffle:
        random.shuffle(sample_files)
    X_data = np.zeros((len(sample_files),max_id))
    Y_data = np.zeros((len(sample_files),))
    for indx in range(len(sample_files)):
        path = sample_files[indx][0]
        y_label = sample_files[indx][1]
        data = pd.read_csv(path,sep="\t").values[0:num_significant]

        for genome in data:
            id = int(str(genome[0]).split()[1])
            log_fc= float(genome[1])
            X_data[indx][id] = log_fc

        Y_data[indx] = y_label
    return X_data,Y_data



test_num = 2
X_data,Y_data = load_dataset([("Data/posdrugs/",1)])
[trainDataMain,means,stds] =normalize_train(X_data[test_num:])
trainLabelsMain = Y_data[test_num:]
testData = X_data[:test_num]
testLabels= Y_data[:test_num]

train_sizes = [len(trainDataMain)]
accTrains = []
accTests = []

for train_size in train_sizes:
    print("Applying trainsize: "+str(train_size))
    trainData=trainDataMain[0:train_size]
    trainLabels = trainLabelsMain[0:train_size]
    method = 3
    if method == 1:

        clf = RandomForestClassifier()

        #scores = cross_val_score(clf,trainData,trainLabels)
        print(trainData, np.max(trainData))
        clf.fit(trainData,trainLabels)
        trainPred = clf.predict(trainData)
        train_accuracy=(accuracy_score(trainPred,trainLabels))

        testPred = clf.predict(testData)
        test_accuracy=(accuracy_score(testPred,testLabels))

    elif method == 2:
        clf = svm.SVC()

        #scores = cross_val_score(clf,trainData,trainLabels)
        clf.fit(trainData,trainLabels)
        trainPred = clf.predict(trainData)
        train_accuracy=(accuracy_score(trainPred,trainLabels))

        testPred = clf.predict(testData)
        test_accuracy=(accuracy_score(testPred,testLabels))
    elif method == 3:
        print(pd.DataFrame(trainData).shape)
        input_dim = trainData.shape[1]
        input_shape= trainData.shape
        hidden_dim = 50


        #zbTrainLabels = np.array(list(map((lambda x:x-1),trainLabels)))
        #zbTestLabels = np.array(list(map((lambda x:x-1),testLabels)))
        zbTrainLabels = trainLabels
        zbTestLabels = testLabels
        # Prepare Theano variables for inputs and targets
        input_var = T.matrix('inputs')
        target_var = T.ivector('targets')
        # Create neural network model
        network = build_mlp(input_var,input_shape,hidden_dim)


        prediction = lasagne.layers.get_output(network)
        loss = lasagne.objectives.categorical_crossentropy(prediction, target_var)
        loss = loss.mean()

        params = lasagne.layers.get_all_params(network, trainable=True)
        updates = lasagne.updates.nesterov_momentum(
            loss, params, learning_rate=0.01, momentum=0.9)


        test_prediction = lasagne.layers.get_output(network, deterministic=True)
        test_loss = lasagne.objectives.categorical_crossentropy(test_prediction,
                                                                target_var)
        test_loss = test_loss.mean()

        test_acc = T.mean(T.eq(T.argmax(test_prediction, axis=1), target_var),
                          dtype=theano.config.floatX)


        train_fn = theano.function([input_var, target_var], loss, updates=updates,allow_input_downcast=True)
        val_fn = theano.function([input_var, target_var], [test_loss, test_acc],allow_input_downcast=True)

        num_epochs=10
        for epoch in range(num_epochs):
            # In each epoch, we do a full pass over the training data:
            train_err = 0
            train_batches = 0
            start_time = time.time()
            for batch in iterate_minibatches(trainData, zbTrainLabels, -1, shuffle=True):
                inputs, targets = batch
                print("inp: ", inputs.shape, " target:", targets.shape)
                train_err += train_fn(inputs, targets)
                train_batches += 1

            # And a full pass over the validation data:
            val_err = 0
            val_acc = 0
            val_batches = 0
            for batch in iterate_minibatches(testData, zbTestLabels, -1, shuffle=False):
                inputs, targets = batch
                err, acc = val_fn(inputs, targets)
                val_err += err
                val_acc += acc
                val_batches += 1

            val_train_err = 0
            val_train_acc = 0
            val_train_batches = 0
            for batch in iterate_minibatches(trainData, zbTrainLabels, -1, shuffle=False):
                inputs, targets = batch
                err, acc = val_fn(inputs, targets)
                val_train_err += err
                val_train_acc += acc
                val_train_batches += 1

            # Then we print the results for this epoch:
            print("Epoch {} of {} took {:.3f}s".format(
                epoch + 1, num_epochs, time.time() - start_time))
            print("  training loss:\t\t{:.6f}".format(train_err / train_batches))
            print("  validation loss:\t\t{:.6f}".format(val_err / val_batches))

            train_accuracy = (val_train_acc / val_train_batches * 100)
            test_accuracy = val_acc/val_batches*100
            print("  train accuracy:\t\t{:.2f} %".format(
                train_accuracy))
            print("  validation accuracy:\t\t{:.2f} %".format(
                test_accuracy))


    accTrains += [train_accuracy]
    accTests += [test_accuracy]

print(train_size)
print(accTrains)
print(accTests)
#pyplot.plot(train_sizes,accTrains,"r",train_sizes,accTests,"b")
#pyplot.show()
#print(scores.mean())